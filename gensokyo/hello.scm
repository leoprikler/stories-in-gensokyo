;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>

;; This program is free software.  It comes without any warranty, to the extent
;; permitted by applicable law.  You can redistribute and/or modify it under
;; the terms of the Do What The Fuck You Want To Public License, Version 2,
;; as published by Sam Hocevar.  See http://www.wtfpl.net/ for more details.

(define-module (gensokyo hello)
  #:use-module (gensokyo)
  #:use-module (sdl2 ttf)
  #:use-module (tsukundere))

(define-public game-name "hello-gensokyo")
(define-public window-title "Hello Gensokyo")

(define-public window-width 1280)
(define-public window-height 720)

(define-public (init!)
  (init-bgi!)
  (init-bgm!)
  (init-sfx!)

  (init-cast! 'renko "蓮子")

  (init-script!
   (lambda () (script-1))
   (default-scene
     #:textbox-position #s32(0 720)
     #:speaker-position #s32(292 540)
     #:text-position #s32(268 570)
     #:menu-position #s32(640 270)
     #:menu-spacing #s32(0 50))))

(define (script-1)
  (define renko (person->procedure (cast-ref 'renko)))

  (script
    (begin
      (background (bgi-ref 'youkai-mountain))
      (add! 'foreground (renko #:position #s32(640 420)
                               #:clothes 'uniform
                               #:mood 'normal))
      (renko "Hello、幻想郷！"))
    *unspecified*))
