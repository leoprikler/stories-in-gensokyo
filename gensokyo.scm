;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public
;; License along with this library; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

(define-module (gensokyo)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 hash-table)
  #:use-module (ice-9 peg)
  #:use-module (ice-9 optargs)
  #:use-module (sdl2)
  #:use-module (sdl2 ttf)
  #:use-module (tsukundere)
  #:use-module ((tsukundere math) #:select (->integer lerp))

  #:export (load-cast
            init-cast! cast-ref
            init-sfx! sfx-ref
            init-bgm! bgm-ref
            init-bgi! bgi-ref
            default-scene
            lerp*))

(define-peg-pattern img.ext body (or ".jpg" ".png"))
(define-peg-pattern snd.ext body (or ".flac" ".mp3" ".ogg" ".wav"))

(define-peg-pattern %id body (+ (or (range #\a #\z) (range #\0 #\9))))
(define-peg-pattern id all %id)
(define-peg-pattern long-id all (and (* (and %id "-")) %id))
(define-peg-pattern image all (and (* (and id "-")) id img.ext))
(define-peg-pattern sound all (and (* (and id "-")) id snd.ext))
(define-peg-pattern bgi all (and "img/bg/" (* (and long-id "/")) long-id img.ext))
(define-peg-pattern bgm all (and "bgm/" (* (and long-id "/")) long-id snd.ext))
(define-peg-pattern sfx all (and "sfx/" (* (and long-id "/")) long-id snd.ext))

(define-peg-pattern clothes all %id)
(define-peg-pattern mood all %id)

(define-peg-pattern cast all (and clothes "-" mood img.ext))

(define bold-font
  ((@ (tsukundere assets) expand-asset-path)
   "gensokyo/font/bold.ttf"))

(define regular-font
  ((@ (tsukundere assets) expand-asset-path)
   "gensokyo/font/regular.ttf"))

(define (load-cast short-name long-name)
  (load-person (string-append "gensokyo/img/" short-name)
               cast
               '(clothes mood)
               #:display-name long-name))

(define (alist->hashq-table! hashq-table alist)
  (for-each
    (lambda (pair)
      (hashq-set! hashq-table (car pair) (cdr pair)))
    alist))

(define *bgi* (make-hash-table))
(define *bgm* (make-hash-table))
(define *cast* (make-hash-table))
(define *sfx* (make-hash-table))

(define (bgi-ref symbol) (hashq-ref *bgi* symbol))
(define (bgm-ref symbol) (hashq-ref *bgm* symbol))
(define (cast-ref symbol) (hashq-ref *cast* symbol))
(define (sfx-ref symbol) (hashq-ref *sfx* symbol))

(define (init-cast! symbol long-name)
  (unless (hashq-get-handle *cast* symbol)
    (hashq-set! *cast* symbol
                (load-cast (symbol->string symbol) long-name))))

(define (init-bgi!)
  (alist->hashq-table! *bgi* (load-images "gensokyo/img/bg" bgi '(long-id))))

(define (init-bgm!)
  (alist->hashq-table! *bgm* (load-music* "gensokyo/bgm" bgm '(long-id))))

(define (init-sfx!)
  (alist->hashq-table! *sfx* (load-sounds "gensokyo/sfx" sfx '(long-id))))

(define* (default-scene #:key textbox-position
           text-position speaker-position
           menu-position menu-spacing)
  (let ((scene (make-scene '(background foreground text menu))))
    (scene-add! scene 'text
                (texture->entity (load-image "gensokyo/img/textbox.png")
                                 #:position textbox-position
                                 #:anchor 'bottom-left))
    (scene-add! scene 'text
                (make-text "" (load-font bold-font 24)
                           #:name 'speaker #:position speaker-position))

    (scene-add! scene 'text
                (make-text "" (load-font regular-font 20)
                           #:name 'text #:position text-position))

    (init-menu! #:scene scene
                #:position menu-position
                #:spacing menu-spacing
                #:button
                (make-button
                 `((none . ,(load-image "gensokyo/img/button.png"))
                   (selected . ,(load-image "gensokyo/img/button-selected.png")))
                 `((none . ,(make-color 255 255 255 255))))
                #:text-font (load-font regular-font 20))
    scene))

(define (lerp* before after alpha)
  (let ((after (array-copy after)))
    (array-map! after
                (lambda (b a)
                  (->integer (lerp b a alpha)))
                before after)
    after))
